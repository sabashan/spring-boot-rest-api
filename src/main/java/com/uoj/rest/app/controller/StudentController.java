package com.uoj.rest.app.controller;

import com.uoj.rest.app.model.Student;
import com.uoj.rest.app.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StudentController {

    @Autowired
    private StudentService studentService;

    @GetMapping("/student/{id}")
    public Student getStudentById(@PathVariable int id) {
        return studentService.getStudent(id);
    }

    @GetMapping("/student/name/{name}")
    public Student findByName(@PathVariable String name) {
        return studentService.getStudent(name);
    }

    @GetMapping("/getAllStudents")
    public List<Student> readAll() {
        return studentService.getAll();
    }

    @PostMapping("/createStudent")
    public Student saveStudent(@RequestBody Student student) {
        return studentService.addStudent(student);
    }


    @PostMapping("/createStudents")
    public List<Student> saveStudents(@RequestBody List<Student> students) {
        return studentService.addStudents(students);
    }

    @DeleteMapping("/removeStudent/{id}")
    public String delete(@PathVariable int id) {
        return studentService.deleteStudent(id);
    }

    @PutMapping("updateStudent/{id}")
    public Student update(@RequestBody Student student, @PathVariable int id) {
        return studentService.updateStudent(id, student);
    }

}
