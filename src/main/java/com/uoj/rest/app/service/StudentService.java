package com.uoj.rest.app.service;

import com.uoj.rest.app.model.Student;
import com.uoj.rest.app.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;



    public Student getStudent(int id) {
        return studentRepository.findById(id).get();
    }

    public Student getStudent(String name) {
        return studentRepository.findByName(name);
    }

    public List<Student> getAll() {
        return studentRepository.findAll();
    }

    public Student addStudent(Student student) {
        return studentRepository.save(student);
    }

    public List<Student> addStudents(List<Student> students) {
        return studentRepository.saveAll(students);
    }

    public String deleteStudent(int id) {
        studentRepository.deleteById(id);
        return "Student removed - " + id;
    }


    public Student updateStudent(int id, Student student) {

        Student oldStudentData = studentRepository.getById(id);

        oldStudentData.setAddress(student.getAddress());
        oldStudentData.setMarks(student.getMarks());
        oldStudentData.setName(student.getName());

        return studentRepository.save(oldStudentData);

    }
}
