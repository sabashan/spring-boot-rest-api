package com.uoj.rest.app.repository;

import com.uoj.rest.app.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StudentRepository extends JpaRepository <Student, Integer> {

    Student findByName(String name);

    List<Student> findByAddress(String address);
}
